import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { product } from './model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private addedProductSubject: BehaviorSubject<any>;
  constructor(private httpClient:HttpClient) { 
    this.addedProductSubject = new BehaviorSubject<any>([]);
  }

  addToCast(products:any){
    this.addedProductSubject.next(products);
  }

  public getProducts (): product[] {
    return this.addedProductSubject.value;
  }

  // getProduct(){
  //   const apiUrl = 'https://steelsoftware.azurewebsites.net/api/FresherFPT';
  //   return this.httpClient.get(apiUrl);
  // }

  getProduct():Observable<HttpResponse<product[]>>{
    const apiUrl = 'https://steelsoftware.azurewebsites.net/api/FresherFPT';
    const options = {
      header: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl,options);
  }


}
