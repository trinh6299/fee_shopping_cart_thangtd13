import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  
  constructor(private httpClient:HttpClient) {

   }

  submitCart(data:any) {
    const body = JSON.stringify(data);
    const apiUrl = 'https://steelsoftware.azurewebsites.net/api/FresherFPT/CheckOut';
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return this.httpClient.post<any>(apiUrl,body,options);
  }
}
