import { Component, EventEmitter, OnInit } from '@angular/core';
import { product } from 'src/app/shared/model';
import { ProductService } from 'src/app/shared/product.service';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {
  products:any;
  productAdded: product[] = [];
  constructor(private productService:ProductService) {
    
  }

  ngOnInit():void{
    this.getProducts();
  }

  getProducts(){
    this.productService.getProduct().subscribe(res=>{
      console.log(res);
      if(res.status==200){
        this.products = res.body;
      }
      else{
        alert('Get data from server failed');
      }
    })
  }

  addToCart(product:product){
    var obj:product = {
      id: product.id,
      productName: product.productName,
      quantity: 1,
      price: product.price,
      promotionPrice: product.promotionPrice,
      image: product.image
    }
    if(sessionStorage.products){
      this.productAdded = JSON.parse(sessionStorage.products);
    }
    
    var existed = this.productAdded.find(x=>x.id == product.id);
    if (existed) {
      existed.quantity +=1;
    } else {
      this.productAdded.push(obj);
    }
    this.productService.addToCast(this.productAdded);
    sessionStorage.products = JSON.stringify(this.productAdded);
    alert('Đã thêm sản phẩm ' + product.productName);
  }
}
