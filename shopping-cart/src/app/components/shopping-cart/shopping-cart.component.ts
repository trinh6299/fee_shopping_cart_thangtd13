import { Component, OnInit } from '@angular/core';
import { product } from 'src/app/shared/model';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { CartService } from 'src/app/shared/cart.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  addedProduct: any[] = [];
  showReceipt = false;
  cartForm = new FormGroup({
    userName: new FormControl('',Validators.required),
    phoneNumber: new FormControl('',Validators.required),
    address: new FormControl('',Validators.required)
  })
  constructor(private cartService: CartService ) { 

  }

  ngOnInit(): void {
    // console.log(this.productService.getProducts())
    if (sessionStorage.products) {
      this.addedProduct = JSON.parse(sessionStorage.products);
    }
  }

  delete(index: number) {
    this.addedProduct.splice(index,1);
    sessionStorage.products = JSON.stringify(this.addedProduct);
  }
  
  change(): void {
    sessionStorage.products = JSON.stringify(this.addedProduct);
  }
  getTotalMoney() {
    var sum = 0;
    for(let item of this.addedProduct){
      sum += item.quantity * item.price * (100 - item.promotionPrice)/100;
    }
    return sum;
  }
  submit(){
    var dataCart = {...this.cartForm.value};
    dataCart.products = this.addedProduct;
    this.cartService.submitCart(dataCart).subscribe(res=>{
      alert("Cart submit success!");
      this.showReceipt = true;
    }, error => {
      alert("Submit fail");
    });
  }
}
